const line = require('@line/bot-sdk');
const express = require('express');
const dotenv = require('dotenv');
dotenv.config();

// google api
const fs = require('fs');
const readline = require('readline');
const {google} = require('googleapis');

const SCOPES = ['https://mail.google.com/',
            'https://www.googleapis.com/auth/gmail.modify',
            'https://www.googleapis.com/auth/gmail.compose',
            'https://www.googleapis.com/auth/gmail.send'];

const TOKEN_PATH = 'googleapi/token.json';

// create Express app
const app = express();

// create LINE SDK config from env variables
const config = {
    channelAccessToken : '1d7tuz9AreAs+iSURGZoNSjnDTXc9uglubbg995YHhPhAqdZXx17BvHfdB5CE7Rq648WqPoEFxlb2t98t9HOe7jvW6S/UP4l+cZCMV9ljTZHddOjgo2RdYQcG3z3TvBjyBL+Sq3bC2M3b7zNYA37iwdB04t89/1O/w1cDnyilFU=',
    channelSecret : '39e7192989010f8740c648dffb92db39',
};

//getting message
app.get('/', (req, res) => {
    console.log('Welcome to heroku');
    return res.end();
});

// webhook callback
app.post('/webhook', line.middleware(config), (req, res) => {

    if (req.body.destination) {
        console.log("Destination ID: " + req.body.destination);
    }

    // req.body.events should be an array of events
    if (!Array.isArray(req.body.events)) {
        return res.status(400).end();
    }

    Promise
    .all(req.body.events.map(handleEvent))
    .then((result) => res.json(result))
    .catch((err) => {
        console.error(err);
        res.status(500).end();
    });
});

// create Client
const client = new line.Client(config);

// simple reply function
const replyText = (token, texts) => {
    texts = Array.isArray(texts) ? texts : [texts];
    return client.replyMessage(
      token,
      texts.map((text) => ({ type: 'text', text }))
    );
};

// callback function to handle a single event
function handleEvent(event) {
    if (event.replyToken && event.replyToken.match(/^(.)\1*$/)) {
        return console.log("Test hook recieved: " + JSON.stringify(event.message));
    }

    // handle webhook event object
    switch(event.type){
        case 'message':
            console.log("Get message event");
        case 'follow':
            return replyText(event.replyToken, 'Got follow event');
        case 'unfollow':
            return console.log(`Unfollowed this bot: ${JSON.stringify(event)}`);
        case 'join':
            return replyText(event.replyToken, `Joined Event ${event.source.type}`);
        case 'leave':
            return console.log(`Left Event: ${JSON.stringify(event)}`);
        case 'memberJoined':
            return replyText(event.replyToken, `Memeber Joined Event ${event.source.type}`);
        case 'memberLeft':
            return console.log(`Member Left Event: ${JSON.stringify(event)}`);
        default:
            return console.log(`Event: ${JSON.stringify(event.type)}`);

    }
}


// send mail
app.get('/send_mail', () => {
    fs.readFile('googleapi/credentials.json', (err, content) => {
        if (err) return console.log('Error loading client secret file:', err);
        // Authorize a client with credentials, then call the Gmail API.
        authorize(JSON.parse(content), sendMail);
    });
});

function authorize(credentials, callback) {
    const {client_secret, client_id, redirect_uris} = credentials.installed;
    const oAuth2Client = new google.auth.OAuth2(client_id, client_secret, redirect_uris[0]);

    // Check if we have previously stored a token.
    fs.readFile(TOKEN_PATH, (err, token) => {
        if (err) return getNewToken(oAuth2Client, callback);
        oAuth2Client.setCredentials(JSON.parse(token));
        callback(oAuth2Client);
    });
}

/**
 * Get and store new token after prompting for user authorization, and then
 * execute the given callback with the authorized OAuth2 client.
 * @param {google.auth.OAuth2} oAuth2Client The OAuth2 client to get token for.
 * @param {getEventsCallback} callback The callback for the authorized client.
 */
function getNewToken(oAuth2Client, callback) {
    const authUrl = oAuth2Client.generateAuthUrl({
        access_type: 'offline',
        scope: SCOPES,
    });
    console.log('Authorize this app by visiting this url:', authUrl);
    const rl = readline.createInterface({
        input: process.stdin,
        output: process.stdout
    });

    rl.question('Enter the code from that page here: ', (code) => {
        rl.close();
        oAuth2Client.getToken(code, (err, token) => {
            if (err) return console.error('Error retrieving access token', err);
            oAuth2Client.setCredentials(token);
            // Store the token to disk for later program executions
            fs.writeFile(TOKEN_PATH, JSON.stringify(token), (err) => {
                if (err) return console.error(err);
                console.log('Token stored to', TOKEN_PATH);
            });
            callback(oAuth2Client);
        });
    });
}

function sendMail(auth) {
    console.log('auth', auth);
    const gmail = google.gmail({
        version: 'v1', 
        auth: auth,
    });

    const subject = '🤘 Hello 🤘';
    const utf8Subject = `=?utf-8?B?${Buffer.from(subject).toString('base64')}?=`;
    const messageParts = [
        'From: Me <chanthem74@gmail.com>',
        'To: SonChanthem <sonchanthem@gmail.com>',
        'Content-Type: text/html; charset=utf-8',
        'MIME-Version: 1.0',
        `Subject: ${utf8Subject}`,
        '',
        'This is a message just to say hello.',
        'So... <b>Hello!</b>  🤘❤️😎',
    ];
  const message = messageParts.join('\n');

  // The body needs to be base64url encoded.
  const encodedMessage = Buffer.from(message)
    .toString('base64')
    .replace(/\+/g, '-')
    .replace(/\//g, '_')
    .replace(/=+$/, '');

    gmail.users.messages.send({
        userId: 'me',
        requestBody: {
            raw: encodedMessage,
        },
    }, (err, res) => {
        if (err) return console.log('API send mail error:' + err);
        return res.data;
    });
}

const baseURL = process.env.BASE_URL;
const port = process.env.PORT;

app.listen(port, () => {
    if(baseURL) {
        console.log(`listening on ${baseURL}:${port}`);
    } else {
        console.log("It seems that BASE_URL is not set. Connecting to ngrok...")
    }
});